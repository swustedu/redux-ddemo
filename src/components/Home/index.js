import React, {
  useState
} from 'react';
import store from '../../store/index';

const Home = () => {
  const [num, updatanum] = useState(store.getState().num);
  const upstateNum = () => {
    updatanum(store.getState().num);
  };
  store.subscribe(upstateNum.bind(this));
  return <h2>{num}</h2>;
};

export default Home;
