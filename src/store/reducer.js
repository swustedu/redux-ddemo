const initstate = {
  num: 10086,
};

const reducer = (state = initstate, action) => {
  let numState = JSON.parse(JSON.stringify(state));
  switch (action.type) {
    case 'addNum':
      return { num: numState.num + 1 };
    case 'misNum':
      numState.num--;
      return numState;
    default:
      return state;
  }
};

export default reducer;
