// context.js
import React from 'react'
let { Consumer, Provider } = React.createContext(null);
export {
    Consumer,
    Provider
}