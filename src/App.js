import React, { Component } from 'react';
import { HashRouter as Router, Link, Route } from 'react-router-dom';
import store from './store/index';
import { connect } from 'react-redux';
import { Provider } from './store/context';
import Home from './components/Home';

const Homes = () => (
  <div>
    <h2>Home</h2>
  </div>
)

const About = () => (
  <div>
    <h2>About</h2>
  </div>
)

const Product = () => (
  <div>
    <h2>Product</h2>
  </div>
)

const App = (props) => {
  console.log(props);
  const handleAdd = (type) => {
    store.dispatch({
      type: type,
    });
  };

  return (
    <Provider>
      <Home />
      <div className='App'>
        <button className='btn' onClick={handleAdd.bind(this, 'addNum')}>
          加 1 操作
        </button>
        <button className='btn' onClick={handleAdd.bind(this, 'misNum')}>
          减 1 操作
        </button>
      </div>
      <Router>
        <div className="App">
          <Link to="/">Home</Link>
          <Link to="/About">About</Link>
          <Link to="/Product">Product</Link>
          <Route path="/" exact component={Homes}></Route>
          <Route path="/about" component={About}></Route>
          <Route path="/product" component={Product}></Route>
        </div>
      </Router>
    </Provider>
  );
};

export default connect(
  (state) => {
    return {
      a: 1,
    };
  },
  (dispatch) => {
    return {
      handleAdd: () => {
        dispatch({
          type: 'addNum',
          payload: 'helo',
        });
      },
    };
  }
)(App);
